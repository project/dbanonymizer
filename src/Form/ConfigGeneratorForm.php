<?php declare(strict_types=1);

namespace Drupal\dbacg\Form;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dbacg\Generator\DbaConfigGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The DBA config generator form.
 */
class ConfigGeneratorForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function __construct(
    private DbaConfigGenerator $generator,
    private SerializationInterface $serializer,
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dbacg.generator'),
      $container->get('serialization.yaml'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'dbacg_config_generator_form';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, ?FormStateInterface $formState): array {
    $dbaConfig = $this->generator->generate();

    $form['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Generated Config'),
      '#description' => $this->t('Keep in mind that you may still need to polish this generated config to fit your needs. Copy this config and save it as a config file for DB Anonymizer to create database dumps.'),
      '#default_value' => $this->serializer->encode($dbaConfig),
      '#attributes' => [
        'rows' => 40,
      ],
    ];

    return $form;
  }

}
