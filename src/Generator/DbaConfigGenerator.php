<?php declare(strict_types=1);

namespace Drupal\dbacg\Generator;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * The DBA config generator.
 */
class DbaConfigGenerator {

  /**
   * Contains generated site config.
   */
  private array $dbaConfig;

  /**
   * Percentage of data to dump for entity types.
   */
  private array $dataPercentage = [
    'default' => 0.01,
    'entity_info' => -1,
    'field_collection_item' => 0,
    'file' => 0,
    'menu_link_content' => -1,
    'path_alias' => -1,
    'shortcut' => -1,
    'taxonomy_term' => 1,
    'taxonomy_vocabulary' => -1,
  ];

  /**
   * Class constructor.
   */
  public function __construct(
    private Connection $connection,
    private EntityTypeManagerInterface $entityTypeManager,
    private EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private EntityFieldManagerInterface $entityFieldManager,
    private ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * Generates site config for DB Anonymizer.
   *
   * @return array
   *   Site config (to be exported as yaml).
   */
  public function generate(): array {
    $this->generateConfigSkeleton();
    $this->generateConfigTables();
    $this->generateConfigEntities();

    return $this->dbaConfig;
  }

  /**
   * Generates site config skeleton.
   */
  private function generateConfigSkeleton(): void {
    $this->dbaConfig = [
      'name' => $this->configFactory->get('system.site')->get('name'),
      'driver' => $this->connection->driver(),
      'database' => [],
    ];
  }

  /**
   * Generates config for some tables export.
   */
  private function generateConfigTables(): void {
    $this->dbaConfig['database']['tables'][$this->table('batch')] = 0;
    $this->dbaConfig['database']['tables'][$this->table('main_menu_link_content_revision')] = 1;
    $this->dbaConfig['database']['tables'][$this->table('main_menu_link_content_field_revision')] = 1;
    $this->dbaConfig['database']['tables'][$this->table('queue')] = 0;
    $this->dbaConfig['database']['tables'][$this->table('sessions')] = 0;
    $this->dbaConfig['database']['tables'][$this->table('watchdog')] = 0;
    $this->dbaConfig['database']['tables']['cache_'] = [
      'table_regex' => '/^' . $this->table('cache') . '.*/',
      'get' => 0,
    ];
    $this->dbaConfig['database']['tables']['_revision'] = [
      'table_regex' => '/^.*_revision.*/',
      'get' => 0,
    ];
    $this->dbaConfig['database']['tables']['search_'] = [
      'table_regex' => '/^' . $this->table('search_') . '.*/',
      'get' => 0,
    ];
  }

  /**
   * Generates config for drupal entities.
   */
  private function generateConfigEntities(): void {
    $entityTypes = $this->entityTypeManager->getDefinitions();
    ksort($entityTypes);

    // Go through all entity types.
    foreach ($entityTypes as $entityType) {
      if (!$entityType instanceof ContentEntityType) {
        continue;
      }

      // Call custom entity type config generator (if exists).
      $method = 'generateConfigEntity' . str_replace('_', '', ucwords($entityType->id(), '_'));
      if (method_exists($this, $method)) {
        $this->{$method}($entityTypes, $entityType->id());
      }
      elseif ($this->shouldExportEntity($entityType->id())) {
        $this->generateConfigEntity($entityTypes, $entityType->id());
      }

      // Call custom entity type anonymization config generator.
      $method = 'generateAnonymizationConfig' . str_replace('_', '', ucwords($entityType->id(), '_'));
      if (method_exists($this, $method)) {
        $this->{$method}($entityTypes, $entityType->id());
      }
    }
  }

  /**
   * Generates config for Taxonomy Term entity type.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   */
  private function generateConfigEntityTaxonomyTerm(array $entityTypes, string $entityTypeId): void {
    $entityType = $entityTypes[$entityTypeId];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityTypeId);

    foreach ($bundles as $bundleId => $label) {
      $entityConfig = [
        'table' => $this->table($entityType->getDataTable()),
        'get' => $this->getPercentage($entityTypeId),
        'where' => [
          $entityType->getKey('bundle') => $bundleId,
        ],
        'fields' => [
          'id' => $entityType->getKey('id'),
        ],
      ];

      $this->appendEntityFieldsConfig($entityTypes, $entityTypeId, $bundleId, $entityConfig);
      $this->appendEntityExtraRelationsConfig($entityTypes, $entityTypeId, $bundleId, $entityConfig);
      $this->dbaConfig['database']['entities'][$entityTypeId . '__' . $bundleId] = $entityConfig;
    }
  }

  /**
   * Generates config for File entity type.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   */
  private function generateConfigEntityFile(array $entityTypes, string $entityTypeId): void {
    $entityType = $entityTypes[$entityTypeId];

    $entityConfig = [
      'get' => $this->getPercentage($entityTypeId),
      'fields' => [
        'id' => $entityType->getKey('id'),
      ],
    ];
    $entityConfig['relations'][$this->table('file_usage')] = [
      'where' => [
        $entityType->getKey('id') => '%' . $entityType->getKey('id'),
      ],
    ];

    $this->dbaConfig['database']['entities'][$this->table($entityType->getBaseTable())] = $entityConfig;
  }

  /**
   * Generates config for User entity type.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   */
  private function generateConfigEntityUser(array $entityTypes, string $entityTypeId): void {
    $entityType = $entityTypes[$entityTypeId];

    // Generate config to always have users with uid=0 and uid=1.
    $entityConfig = $this->generateConfigEntity($entityTypes, $entityTypeId, [
      'get' => 1,
      'where' => [$entityType->getKey('id') => [1, '<=']],
      'where__no_bundle' => TRUE,
      'do_not_add' => TRUE,
    ]);
    $this->dbaConfig['database']['entities'][$entityTypeId . '__drupal'] = $entityConfig;

    // Also append config to get normal percentage of random users.
    $entityConfig['get'] = $this->getPercentage($entityTypeId);
    $entityConfig['where'] = [$entityType->getKey('id') => [1, '>']];
    $this->dbaConfig['database']['entities'][$entityTypeId] = $entityConfig;
  }

  /**
   * This is generic method to generate config for some entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   * @param array $params
   *   Parameters for entity config.
   *
   * @return array
   *   Generated config for the last entity bundle.
   */
  private function generateConfigEntity(array $entityTypes, string $entityTypeId, array $params = []): array {
    $entityType = $entityTypes[$entityTypeId];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityTypeId);
    $bundleId1 = array_keys($bundles)[0] ?? '';
    $hasBundles = count($bundles) > 1 || $bundleId1 !== $entityTypeId;
    $table = $entityType->getDataTable() ?: $entityType->getBaseTable();

    if (!$table) {
      return [];
    }

    foreach ($bundles as $bundleId => $label) {
      $entityConfig = [
        'table' => $this->table($table),
        'get' => $params['get'] ?? $this->getPercentage($entityTypeId),
      ];

      if (!empty($params['where'])) {
        $entityConfig['where'] = $params['where'];
      }
      if ($hasBundles && empty($params['where__no_bundle']) && $entityType->getKey('bundle')) {
        $entityConfig['where'][$entityType->getKey('bundle')] = $bundleId;
      }

      $entityConfig['fields']['id'] = $entityType->getKey('id');

      $this->appendEntityFieldsConfig($entityTypes, $entityTypeId, $bundleId, $entityConfig);
      $this->appendEntityExtraRelationsConfig($entityTypes, $entityTypeId, $bundleId, $entityConfig);

      if (empty($entityConfig['relations'])) {
        unset($entityConfig['fields']);
      }

      if (empty($params['do_not_add'])) {
        $id = !$hasBundles ? $entityTypeId : $entityTypeId . '__' . $bundleId;
        $this->dbaConfig['database']['entities'][$id] = $entityConfig;
      }
    }

    return $entityConfig ?? [];
  }

  /**
   * Adds field config as relations to entity config.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   * @param string $entityBundleId
   *   Current entity bundle id.
   * @param array $entityConfig
   *   Current entity type/bundle config. Field config is added to this config.
   */
  private function appendEntityFieldsConfig(array $entityTypes, string $entityTypeId, string $entityBundleId, array &$entityConfig): void {
    $fields = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $entityBundleId);

    foreach ($fields as $fieldName => $field) {
      $storage = $field->getFieldStorageDefinition();

      if ($field instanceof BaseFieldDefinition
        || $storage instanceof BaseFieldDefinition
      ) {
        continue;
      }

      // Found no simple way to get the table name here, so use this workaround.
      $tableWithoutPrefix = str_replace('.', '__', $storage->getOriginalId());
      $table = $this->table($tableWithoutPrefix);

      $entityConfig['relations'][$table] = [
        'where' => [
          'bundle' => $entityBundleId,
          'entity_id' => '%' . $entityTypes[$entityTypeId]->getKey('id'),
        ],
      ];

      // If field contains just 1 value then add limit to boost
      // the search query.
      if ($storage->getCardinality() === 1) {
        $entityConfig['relations'][$table]['limit'] = 1;
      }

      // Check if a field is a reference to another entity.
      $referencedEntityTypeId = $field->getType() === 'entity_reference'
        ? $field->getSetting('target_type')
        : '';

      // If a field is an entity reference then we need to add a relation
      // to dump related entity as well.
      if ($referencedEntityTypeId && $this->shouldExportEntity($referencedEntityTypeId)) {
        $referencedTable = $entityTypes[$referencedEntityTypeId]->getDataTable() ?: $entityTypes[$referencedEntityTypeId]->getBaseTable();
        $entityConfig['relations'][$table]['relations'][$fieldName] = [
          'table' => $this->table($referencedTable),
          'is_entity' => 1,
          'values' => [
            'id' => '%' . $fieldName . '_target_id',
            'type' => $referencedEntityTypeId,
          ],
          'fields' => [
            'id' => $entityTypes[$referencedEntityTypeId]->getKey('id'),
            'bundle' => $entityTypes[$referencedEntityTypeId]->getKey('bundle'),
          ],
        ];
      }
    }
  }

  /**
   * Adds extra relations config to entity config.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   * @param string $entityBundleId
   *   Current entity bundle id.
   * @param array $entityConfig
   *   Current entity type/bundle config. Relations are added to this config.
   */
  private function appendEntityExtraRelationsConfig(array $entityTypes, string $entityTypeId, string $entityBundleId, array &$entityConfig): void {
    $relations = $this->getEntityExtraRelations($entityTypes, $entityTypeId);
    foreach ($relations as $table => $config) {
      $entityConfig['relations'][$this->table($table)] = $config;
    }
  }

  /**
   * Returns extra relations for the entity type.
   *
   * Extra relations are tables related to the entity type, besides the
   * field tables.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   *
   * @return array
   *   Config for extra relations that can be appended to entity type config.
   */
  private function getEntityExtraRelations(array $entityTypes, string $entityTypeId): array {
    $relations = [];
    $entityType = $entityTypes[$entityTypeId];
    $baseTable = $entityType->getBaseTable();
    $dataTable = $entityType->getDataTable();

    if ($dataTable && $baseTable) {
      $relations[$baseTable] = [
        'where' => [
          $entityType->getKey('id') => '%' . $entityType->getKey('id'),
        ],
      ];
    }

    switch ($entityTypeId) {
      case 'node':
        $relations['node_access'] = [
          'where' => [
            $entityType->getKey('id') => '%' . $entityType->getKey('id'),
          ],
        ];
        break;

      case 'user':
        $relations['user__roles'] = [
          'where' => [
            $entityType->getKey('id') => '%' . $entityType->getKey('id'),
          ],
        ];
        break;
    }

    return $relations;
  }

  /**
   * Generates config for File entity table anonymization.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   */
  private function generateAnonymizationConfigFile(array $entityTypes, string $entityTypeId): void {
    $this->dbaConfig['database']['anonymization'][$this->table($entityTypes[$entityTypeId]->getBaseTable())] = [
      'where' => [
        'uri' => ['!^private://.*!', 'regex'],
      ],
      'fields' => [
        'filename' => [
          'method' => 'Faker::lexify',
          'args' => ['??????????.txt'],
        ],
        'uri' => [
          'method' => 'concat',
          'args' => [
            'private://',
            '%filename',
          ],
        ],
        'filemime' => 'text/plain',
      ],
    ];
  }

  /**
   * Generates config for User entity table anonymization.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface[] $entityTypes
   *   Entity types data.
   * @param string $entityTypeId
   *   Current entity type id.
   */
  private function generateAnonymizationConfigUser(array $entityTypes, string $entityTypeId): void {
    $this->dbaConfig['database']['anonymization'][$this->table($entityTypes[$entityTypeId]->getDataTable())] = [
      'where' => [
        'uid' => [0, '>'],
      ],
      'fields' => [
        'name' => [
          'method' => 'concat',
          'args' => [
            ['method' => 'Faker::word'],
            '_',
            '%uid',
          ],
        ],
        'pass' => '-',
        'mail' => [
          'method' => 'concat',
          'args' => [
            '%name',
            '@test.test',
          ],
        ],
        'init' => '%mail',
      ],
    ];
  }

  /**
   * Returns the percentage of data to dump for entity type.
   *
   * @param string $entityTypeId
   *   Entity type id.
   *
   * @return float
   *   The number from 0 to 1, where 1 means 100%.
   */
  private function getPercentage(string $entityTypeId) {
    return $this->dataPercentage[$entityTypeId] ?? $this->dataPercentage['default'];
  }

  /**
   * Checks if entity type config should be exported.
   *
   * @param string $entityTypeId
   *   Entity type id.
   *
   * @return bool
   *   TRUE - if entity type config should be exported, or FALSE otherwise.
   */
  private function shouldExportEntity(string $entityTypeId): bool {
    return $this->getPercentage($entityTypeId) >= 0;
  }

  /**
   * Returns prefixed table name.
   *
   * @param string $table
   *   Table name without a prefix.
   *
   * @return string
   *   Table name with a prefix.
   */
  private function table(string $table): string {
    return $this->connection->getPrefix() . $table;
  }

}
