# DB Anonymizer Config Generator

This is a DB Anonymizer config generator for drupal sites.
[DB Anonimizer](https://github.com/iacmchs/dba) is a tool that can:
1. Do partial data export from DB.
2. Anonimize data during the export.

## Usage

1. Install and enable this module as usual.
2. Go to `/admin/dbanonymizer-config-generator` page, where you can copy
generated config for your site.

Keep in mind that you may still need to change this config to fit your needs,
but this would be a good starting point as generator had done all the hard
lifting for you. Think of some custom db tables (and their relations) that
your site may have, consider adding them to the config as well. You might also
want to add anonymization config for some tables (e.g. fields).

Then you can use that config as usual,
see [DB Anonimizer](https://github.com/iacmchs/dba) readme for more details.

## Notes

1. This config generator assumes that data from drupal system tables is fully
dumped.
2. It also assumes that data from the tables related to the following entities
is fully dumped:
   - Menu and menu items
   - Views
3. This config generator has basic support for tables (entities and/or
relations) coming from the contributed modules. But you should check that
config generated for them looks fine and add/update it manually if needed.

Please pay attention to the config generated for entities like:
- Blocks
- Comments
- Taxonomy terms

Chances are you do not need it and can just remove it. Consider to keep it in
case if terms (or other entities) has some fields of `entity_reference` type
(and related tables should not be fully dumped).

If you want to add more features to this module (e.g. support more entities)
feel free to contribute.

## Troubleshooting

Check the 'Import data' and 'Troubleshooting' sections of
[DB Anonimizer](https://github.com/iacmchs/dba) readme first. If you have
broken sequence issue then you can use `drush sql:fix:sequences` command
provided by this module.
